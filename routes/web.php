<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\MovieController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MovieController::class, 'index'])->name('index');
Route::get('/categories', [MovieController::class, 'categories'])->name('categories');
Route::get('/movie-detail/{id}', [MovieController::class, 'movieDetail'])->name('movie-detail');
Route::get('/movie-watch/{id}/{episodeId?}', [MovieController::class, 'movieWatch'])->name('movie-watch');
Route::get('/search-movie-by-dwm/', [MovieController::class, 'searchMovieBySearch'])->name('movie-search-by-dwm');
Route::post('add-rating', [MovieController::class, 'addRating'])->name('add-rating');

Route::get('/create-movie', [MovieController::class, 'storeMovie'])->name('store-movie');
Route::post('/create-movie', [MovieController::class, 'createMovie'])->name('create-movie');

Route::group(['prefix' => 'payment'], function () {
    Route::get('checkout', [MovieController::class, 'checkout'])->name('payment.checkout');
    Route::post('checkout', [MovieController::class, 'payment'])->name('payment');
    Route::get('/checkout-success', [MovieController::class, 'paymentSuccess'])->name('payment.success');
    Route::get('/checkout-cancel', [MovieController::class, 'paymentCancel'])->name('payment.cancel');
    Route::get('getBalanceStripe', [MovieController::class, 'getBalanceStripe'])->name('payment.balance');
});

Route::get('/ws', function (){
    return view('websocket');
});

Route::post('comment', [CommentController::class, 'postComment']);
Route::get('get-comment', [CommentController::class, 'getComments']);

Route::get('login', [LoginController::class, 'viewLogin'])->name('login.view');
Route::post('login', [LoginController::class, 'login'])->name('login');
Route::get('register', [LoginController::class, 'viewRegister'])->name('register.view');
Route::post('register-post', [LoginController::class, 'registerPost'])->name('register');

Route::get('logout', function () {
    Auth::logout();
    return back();
});


