<?php

namespace App\Providers;

use App\Models\CategoryMovie;
use App\Models\CountryMovie;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;


class ViewShareProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $categories = CategoryMovie::all();
        $countrys = CountryMovie::all();

        View::share(['categories' => $categories, 'countrys' => $countrys]);
    }
}
