<?php

namespace App\Listeners;

use App\Models\MovieComment;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CommentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        MovieComment::create([
            'movie_id' => $event->movieId,
            'user_id' => $event->user->id,
            'content' => $event->message
        ]);
    }
}
