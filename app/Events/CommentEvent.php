<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CommentEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $message;
    public $movieId;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $message, $movieId)
    {
        $this->user = $user;
        $this->message = $message;
        $this->movieId = $movieId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('chat_' . $this->movieId);
    }

    public function broadcastWith(): array
    {
        Carbon::setLocale('vi');
        $now = Carbon::parse(now());
        $timeAgo = $now->diffForHumans(now());
        $this->user->image_url = 'https://tuantv111.s3.ap-southeast-1.amazonaws.com/image_user/5034901-200.png';
        return [
            'user' => $this->user,
            'content' => $this->message,
            'time' => $timeAgo,
            'movieId' => $this->movieId
        ];
    }
}
