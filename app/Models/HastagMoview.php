<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HastagMoview extends Model
{
    use HasFactory;

    protected $table = 'hastag_movies';

    public function movies()
    {
        return $this->hasMany(Movie::class, 'hastag_movie_id', 'id');
    }
}
