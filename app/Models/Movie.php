<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;
    protected $guarded = [];


    protected $table = 'movies';

    public function categoryMovie()
    {
        return $this->belongsTo(CategoryMovie::class, 'category_id', 'id');
    }

    public function hastagMovie()
    {
        return $this->belongsTo(HastagMoview::class, 'hastag_movie_id', 'id');
    }

    public function countryMovie()
    {
        return $this->belongsTo(CountryMovie::class, 'country_id', 'id');
    }

    public function episodes()
    {
        return $this->hasMany(Episode::class, 'movie_id', 'id');
    }

    public function retings()
    {
        return $this->hasMany(Rateting::class, 'movie_id', 'id');
    }
}
