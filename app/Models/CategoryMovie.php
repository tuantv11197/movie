<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryMovie extends Model
{
    use HasFactory;
    protected $table = 'category_movies';

    public function movies()
    {
        return $this->hasMany(Movie::class, 'category_id', 'id');
    }
}
