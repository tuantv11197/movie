<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountryMovie extends Model
{
    use HasFactory;
    protected $table = 'coutry_movies';

    public function movies()
    {
        return $this->hasMany(Movie::class, 'movie_id', 'id');
    }
}
