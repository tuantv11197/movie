<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function viewLogin()
    {
        return view('auth.login');
    }

    public function viewRegister()
    {
        return view('auth.register');
    }
    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request['email'], 'password' => $request['Password']])) {
            return redirect()->route('index');
        }
        Session::flash('error', 'Email hoặc mật khẩu không đúng!');
        return redirect()->route('login.view');
    }

    public function registerPost(RegisterRequest $request)
    {
        User::create([
            'name' => $request['username'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        return response()->json(['status' => true, 'data'=> []]);
    }
}
