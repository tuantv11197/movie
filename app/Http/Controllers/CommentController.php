<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\MovieComment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function getComments(Request $request)
    {
        Carbon::setLocale('vi');
        $movieComments = MovieComment::query()->where('movie_id', $request['movie_id'])->with('user')->orderBy('id', 'asc')->get();
        $movieComments = $movieComments->map(function ($item) {
            $timeAgo = Carbon::parse($item->created_at);
            $timeAgo = $timeAgo->diffForHumans(now());
            $item->user->image_url = 'https://tuantv111.s3.ap-southeast-1.amazonaws.com/image_user/5034901-200.png';
            $item->time = $timeAgo;
            return $item;
        });
        return response()->json([
            'status' => true,
            'data' => $movieComments
        ]);
    }
    public function postComment(CommentRequest $request)
    {
        $user = \auth()->user();
        if (!$user) {
            return response()->json([
               'status' => false,
               'messages' => 'Bạn chưa đăng nhập, vui lòng đăng nhập để có thể bình luận!',
                'code' => 403
            ], 422);
        }
        $comment = $request['content'];
        $movieId = $request['movie_id'];
        event(new \App\Events\CommentEvent($user, $comment, $movieId));
    }
}
