<?php

namespace App\Http\Controllers;

use App\Models\CategoryMovie;
use App\Models\CountryMovie;
use App\Models\Episode;
use App\Models\HastagMoview;
use App\Models\Movie;
use App\Models\Rateting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class MovieController extends Controller
{
    public function checkout()
    {
        return view('stripe.subscription');
    }

    public function paymentSuccess()
    {
        return response()->json('thanh toan thanh cong');
    }

    public function payment()
    {
        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));

        $checkout_session = $stripe->checkout->sessions->create([
            'line_items' => [
                [
                    'price_data' => [
                        'currency' => 'usd',
                        'product_data' => [
                            'name' => 'san pham 1',
                        ],
                        'unit_amount' => 2000,
                    ],
                    'quantity' => 1,
                ]
            ],
            'mode' => 'payment',
            'success_url' => route('payment.success'),
            'cancel_url' => route('payment.cancel')
        ]);

        return redirect($checkout_session->url);
    }

    public function getBalanceStripe()
    {
        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
        $balance = $stripe->balance->retrieve([]);
        return response($balance);
    }

    public function addRating(Request $request) {
        logger($request);
        Rateting::create([
            'movie_id' => $request['movie_id'],
            'rating' => $request['index'],
            'user_id' => 1
        ]);
        return response()->json([
            'data' => 'done',
        ]);
    }
    public function storeMovie() {
        $cateMovies = CategoryMovie::all();
        $hastagMovies = HastagMoview::all();
        $countrys = CountryMovie::all();
        return view('admin.movie.create', compact('cateMovies', 'hastagMovies', 'countrys'));
    }
    public function createMovie(Request $request) {
        $data = $request->only([
            'hastag_movie_id', 'category_id', 'name_vn', 'name_en', 'quality', 'is_hot', 'episode_id', 'subscription', 'country_id', 'is_new'
        ]);
        $slug = Str::slug($request['slug']);
        $movie = Movie::query()->where('slug', $slug)->first();
        if (!$movie) {
            $file = $request['image'];
            if($file) {
                $path = Storage::disk(env('STORAGE_DISK', 'public'))->put('movies', $file);
                $image = [
                    'name' => $file->getClientOriginalName(),
                    'path' => $path
                ];
                $data['image'] = 'https://tuantv111.s3.ap-southeast-1.amazonaws.com/' . $path;
            }
            $data['slug'] = $slug;

            $movie = Movie::create($data);
        }
        Episode::create([
            'movie_id' => $movie->id,
            'link_movie' => $request['link_movie'],
            'episode' => $request['episode_id'],
            'is_sub' => $request['is_sub']
        ]);
        return redirect()->route('index');
    }

    public function index(Request $request)
    {
        $search = str_replace('+', '', $request['s']);
        $movies = Movie::query()
            ->when($request['s'], function ($q) use ($search) {
                $q->where('name_vn', 'like', '%' . $search . '%');
            })
            ->when($request['category_id'], function ($q) use ($request) {
                $q->where('category_id', $request['category_id']);
            })
            ->when($request['is_new'], function ($q) use ($request) {
                $q->where('is_new', $request['is_new']);
            })
            ->when($request['country_id'], function ($q) use ($request) {
                $q->where('country_id', $request['country_id']);
            })
            ->when($request['hastag_id'], function ($q) use ($request) {
                $q->whereHas('hastagMovie', function ($q) use ($request) {
                    $q->where('id', $request['hastag_id']);
                });
            })
            ->with(['categoryMovie', 'hastagMovie', 'countryMovie'])->orderBy('created_at', 'desc')->get();
        $phimRap = $movies->filter(function ($item) {
            return $item->hastag_movie_id == 1;
        });
        $phimBo = $movies->filter(function ($item) {
            return $item->hastag_movie_id == 2;
        });

        $phimLe = $movies->filter(function ($item) {
            return $item->hastag_movie_id == 3;
        });
        $searchKeyword = $request['s'];

        return view('pages.home', compact( 'movies', 'phimRap', 'phimBo', 'phimLe', 'searchKeyword'));
    }

    public function categories()
    {
        return view('pages.categories');
    }
    public function movieDetail($movieId)
    {
        $movidetail = Movie::query()->where('id', $movieId)->with(['categoryMovie', 'hastagMovie', 'countryMovie'])->first();
        $movieByCategories = Movie::query()->where('category_id', $movidetail->category_id)->get();
        $rating = round(Rateting::query()->where('movie_id', $movieId)->avg('rating'));

        return view('pages.movieDetail', compact('movidetail', 'movieByCategories', 'rating'));
    }

    public function movieWatch($movieId, $episodeId = '', Request $request)
    {
        $movieTopViews = Movie::query()
            ->when(isset($request['search_view']), function ($q) use ($request) {
                $q->whereDay('updated_at', now()->day)
                    ->orwhereBetween('updated_at', [now()->startOfWeek(), now()->endOfWeek()])
                    ->orWhereBerween('updated_at', [now()->startOfMonth(), now()->endOfMonth()]);
            })
            ->orderBy('view', 'desc')->get();
        $movidetail = Movie::query()->where('id', $movieId)->with(['categoryMovie', 'hastagMovie', 'countryMovie', 'episodes' => function($q) use ($episodeId) {
            $q->when($episodeId, function($q2) use ($episodeId) {
                $q2->where('episode', $episodeId);
            })
            ->select('movie_id', 'link_movie', 'episode')->orderBy('episode', 'asc');
        }])->first();
        $movidetail->update(['view' => $movidetail->view += 1]);
        $episodeId = $movidetail->episodes->pluck('episode');
        $episodes = Episode::query()->where('movie_id', $movieId)->orderBy('episode', 'asc')->pluck('episode');
        $movieByCategories = Movie::query()->where('category_id', $movidetail->category_id)->get();
        return view('pages.watch', compact('movidetail', 'episodes', 'movieByCategories', 'movieTopViews', 'episodeId'));
    }

    public function searchMovieBySearch(Request $request)
    {
        $movieTopViews = Movie::query()
            ->when($request['search_view'] == 'day', function ($q) use ($request) {
                $q->whereDay('updated_at', now()->day);
            })
            ->when($request['search_view'] == 'week', function ($q) use ($request) {
                $q->whereBetween('updated_at', [now()->startOfWeek(), now()->endOfWeek()]);
            })
            ->when($request['search_view'] == 'month', function ($q) use ($request) {
                $q->whereBetween('updated_at', [now()->startOfMonth(), now()->endOfMonth()]);
            })
            ->orderBy('view', 'desc')->limit(10)->get();
        return response()->json([
            'data' => $movieTopViews
        ]);
    }
}
