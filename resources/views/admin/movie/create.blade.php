<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<form style="width: 50%; margin:auto" action="{{ route('create-movie') }}" method="post" enctype="multipart/form-data">
    @csrf
  <div class="mb-3">
    <label for="" class="form-label">Hastag Movie_id</label>
    <select class="form-select" name="hastag_movie_id" aria-label="Default select example">
        <option selected>...</option>
        @foreach($hastagMovies as $hastag)
        <option value="{{ $hastag->id }}">{{ $hastag->name }}</option>
        @endforeach
    </select>
  </div>
  <div class="mb-3">
    <label for="" class="form-label">category_id</label>
    <select class="form-select" name="category_id" aria-label="Default select example">
        <option selected>...</option>
        @foreach($cateMovies as $cate)
        <option value="{{ $cate->id }}">{{ $cate->name }}</option>
        @endforeach
    </select>
  </div>
  <div class="mb-3">
    <label for="" class="form-label">name_vn</label>
    <input type="text" name="name_vn" class="form-control" id="">
  </div>
  <div class="mb-3">
    <label for="" class="form-label">name_en</label>
    <input type="text" name="name_en" class="form-control" id="">
  </div>
  <div class="mb-3">
    <label for="" class="form-label">quality</label>
    <select class="form-select" name="quality" aria-label="Default select example">
        <option selected>...</option>
        <option value="1">HD</option>
        <option value="2">720</option>
        <option value="3">480</option>
    </select>
  </div>
  <div class="mb-3">
    <label for="" class="form-label">is_hot</label>
    <select class="form-select" name="is_hot" aria-label="Default select example">
        <option selected>...</option>
        <option value="1">Hot</option>
        <option value="0">Nomal</option>
    </select>
  </div>
  <div class="mb-3">
    <label for="" class="form-label">Tap phim</label>
    <input type="number" name="episode_id" class="form-control" id="">
  </div>
  <div class="mb-3">
    <label for="" class="form-label">subscription</label>
    <textarea type="text" name="subscription" class="form-control" id=""></textarea>
  </div>
  <div class="mb-3">
    <label for="" class="form-label">slug</label>
    <input type="text" name="slug" class="form-control" id="">
  </div>
  <div class="mb-3">
    <label for="" class="form-label">country_id</label>
    <select class="form-select" name="country_id" aria-label="Default select example">
        <option selected>...</option>
        @foreach($countrys as $coutry)
        <option value="{{ $coutry->id }}">{{ $coutry->name }}</option>
        @endforeach
    </select>
  </div>
  <div class="mb-3">
    <label for="" class="form-label">is_new</label>
    <select class="form-select" name="is_new" aria-label="Default select example">
        <option selected>...</option>
        <option value="1">New</option>
        <option value="0">Nomal</option>
    </select>
  </div>
    <div class="mb-3">
        <label for="" class="form-label">Link movie</label>
        <input type="text" name="link_movie" class="form-control" id="">
    </div>
    <div class="mb-3">
        <label for="" class="form-label">Sub</label>
        <input type="number" name="is_sub" class="form-control" id="">
    </div>
  <div class="mb-3">
    <label for="" class="form-label">image</label>
    <input type="file" name="image" class="form-control" id="">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
