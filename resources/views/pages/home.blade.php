@extends('layout')
@section('content')
<div class="row container" id="wrapper">
            <div class="halim-panel-filter">
               <div id="ajax-filter" class="panel-collapse collapse" aria-expanded="true" role="menu">
                  <div class="ajax"></div>
               </div>
            </div>
            @if(count($movies) > 1)
                <section id="" style="padding:0 15px">
                    <div class="section-heading">
                        <a href="danhmuc.php" title="Phim Chiếu Rạp">
                            <span class="h-text">Phim Hot</span>
                        </a>
                        <ul class="heading-nav pull-right hidden-xs">
                            <li class="section-btn halim_ajax_get_post" data-catid="4" data-showpost="12"
                                data-widgetid="halim-advanced-widget-4" data-layout="6col"><span data-text="Phim hot"></span></li>
                        </ul>
                    </div>
                    <div id="halim-advanced-widget-4-ajax-box" class="halim_box">
                        <div id="halim_related_movies-22" class="owl-carousel owl-theme related-film">
                            @foreach($movies as $movie)
                                <article class="thumb grid-item post-38498">
                                    <div class="halim-item">
                                        <a class="halim-thumb" href="{{ route('movie-detail', $movie->id) }}" title="{{ $movie->name_vn }}">
                                            <figure><img class="lazy img-responsive" src="{{ $movie->image }}"
                                                         alt="{{ $movie->name_vn }}" title="{{ $movie->name_vn }}"></figure>
                                            <span
                                                class="status">{{ $movie->quality == 1 ? 'HD' :( $movie->quality == 2 ? 720 : '480') }}</span><span
                                                class="episode"><i class="fa fa-play" aria-hidden="true"></i>Vietsub</span>
                                            <div class="icon_overlay"></div>
                                            <div class="halim-post-title-box">
                                                <div class="halim-post-title ">
                                                    <p class="entry-title">{{ $movie->name_vn }}</p>
                                                    <p class="original_title">{{ $movie->name_en }}</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </article>
                            @endforeach
                        </div>
                    </div>
                </section>
             @endif
            <script>
                jQuery(document).ready(function ($) {
                    var owl = $('#halim_related_movies-22');
                    owl.owlCarousel({
                        loop: true,
                        margin: 4,
                        autoplay: true,
                        autoplayTimeout: 4000,
                        autoplayHoverPause: true,
                        nav: true,
                        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-angle-right"></i>'],
                        responsiveClass: true,
                        responsive: {0: {items: 2}, 480: {items: 3}, 600: {items: 6}, 1000: {items: 6}}
                    })
                });
            </script>
            @if(count($phimRap))
            <div class="col-xs-12 carausel-sliderWidget">
               <section id="halim-advanced-widget-4">
                  <div class="section-heading">
                     <a href="danhmuc.php" title="Phim Chiếu Rạp">
                     <span class="h-text">Phim Chiếu Rạp</span>
                     </a>
                     <ul class="heading-nav pull-right hidden-xs">
                        <li class="section-btn halim_ajax_get_post" data-catid="4" data-showpost="12" data-widgetid="halim-advanced-widget-4" data-layout="6col"><span data-text="Chiếu Rạp"></span></li>
                     </ul>
                  </div>
                   <div id="halim-advanced-widget-4-ajax-box" class="halim_box">
                       @foreach($phimRap as $movie)
                           <article class="col-md-2 col-sm-4 col-xs-6 thumb grid-item post-38424">
                               <div class="halim-item">
                                   <a class="halim-thumb" href="{{ route('movie-detail', $movie->id) }}" title="GÓA PHỤ ĐEN">
                                       <figure><img class="lazy img-responsive" src="{{ $movie->image }}" alt="GÓA PHỤ ĐEN" title="GÓA PHỤ ĐEN"></figure>
                                       <span class="status">{{ $movie->quality == 1 ? 'HD' :( $movie->quality == 2 ? 720 : '480') }}</span><span class="episode"><i class="fa fa-play" aria-hidden="true"></i>Vietsub</span>
                                       <div class="icon_overlay"></div>
                                       <div class="halim-post-title-box">
                                           <div class="halim-post-title ">
                                               <p class="entry-title">{{ $movie->name_vn }}</p>
                                               <p class="original_title">{{ $movie->name_en }}</p>
                                           </div>
                                       </div>
                                   </a>
                               </div>
                           </article>
                       @endforeach
                   </div>
               </section>
               <div class="clearfix"></div>
            </div>
            @endif
            <main id="main-contents" class="col-xs-12 col-sm-12 col-md-8">
               @if(count($phimBo))
               <section id="halim-advanced-widget-2">
                  <div class="section-heading">
                     <a href="danhmuc.php" title="Phim Bộ">
                     <span class="h-text">Phim Bộ</span>
                     </a>
                  </div>
                  <div id="halim-advanced-widget-2-ajax-box" class="halim_box">
                  @foreach($phimBo as $movie)
                     <article class="col-md-3 col-sm-3 col-xs-6 thumb grid-item post-37606">
                        <div class="halim-item">
                           <a class="halim-thumb" href="{{ route('movie-detail', $movie->id) }}">
                              <figure><img class="lazy img-responsive" src="{{ $movie->image }}" alt="BẠN CÙNG PHÒNG CỦA TÔI LÀ GUMIHO" title="BẠN CÙNG PHÒNG CỦA TÔI LÀ GUMIHO"></figure>
                              <span class="status">TẬP 15</span><span class="episode"><i class="fa fa-play" aria-hidden="true"></i>Vietsub</span>
                              <div class="icon_overlay"></div>
                              <div class="halim-post-title-box">
                                 <div class="halim-post-title ">
                                    <p class="entry-title">{{ $movie->name_vn }}</p>
                                    <p class="original_title">{{ $movie->name_en }}</p>
                                 </div>
                              </div>
                           </a>
                        </div>
                     </article>
                     @endforeach
                  </div>
               </section>
               @endif
               <div class="clearfix"></div>
               @if(count($phimLe))
                  <section id="halim-advanced-widget-2">
                     <div class="section-heading">
                        <a href="danhmuc.php" title="Phim Lẻ">
                        <span class="h-text">Phim Lẻ</span>
                        </a>
                     </div>
                     <div id="halim-advanced-widget-2-ajax-box" class="halim_box">
                        @foreach($phimLe as $movie)
                           <article class="col-md-3 col-sm-3 col-xs-6 thumb grid-item post-37606">
                              <div class="halim-item">
                                 <a class="halim-thumb" href="{{ route('movie-detail', $movie->id) }}">
                                    <figure><img class="lazy img-responsive" src="{{ $movie->image }}" alt="BẠN CÙNG PHÒNG CỦA TÔI LÀ GUMIHO" title="BẠN CÙNG PHÒNG CỦA TÔI LÀ GUMIHO"></figure>
                                    <span class="status">TẬP 15</span><span class="episode"><i class="fa fa-play" aria-hidden="true"></i>Vietsub</span>
                                    <div class="icon_overlay"></div>
                                    <div class="halim-post-title-box">
                                       <div class="halim-post-title ">
                                          <p class="entry-title">{{ $movie->name_vn }}</p>
                                          <p class="original_title">{{ $movie->name_vn }}</p>
                                       </div>
                                    </div>
                                 </a>
                              </div>
                           </article>
                        @endforeach
                     </div>
                  </section>
               @endif
               <div class="clearfix"></div>
               <div class="clearfix"></div>
            </main>
            <aside id="sidebar" class="col-xs-12 col-sm-12 col-md-4">
               <div id="halim_tab_popular_videos-widget-7" class="widget halim_tab_popular_videos-widget">
                  <div class="section-bar clearfix">
                     <div class="section-title">
                        <span>Top Views</span>
                        <ul class="halim-popular-tab" role="tablist">
                           <li role="presentation" class="active">
                              <a class="ajax-tab" role="tab" data-toggle="tab" data-showpost="10" data-type="day">Day</a>
                           </li>
                           <li role="presentation">
                              <a class="ajax-tab" role="tab" data-toggle="tab" data-showpost="10" data-type="week">Week</a>
                           </li>
                           <li role="presentation">
                              <a class="ajax-tab" role="tab" data-toggle="tab" data-showpost="10" data-type="month">Month</a>
                           </li>
                           <li role="presentation">
                              <a class="ajax-tab" role="tab" data-toggle="tab" data-showpost="10" data-type="all">All</a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <section class="tab-content">
                     <div role="tabpanel" class="tab-pane active halim-ajax-popular-post">
                        <div id="load-movie-top-home" class="popular-post">
                           <div v-for="movieTop in JSON.parse(movieTops)" class="item post-37176">
                               <div :data-movie-id="movieTop.id">
                                   <a @click="rediarectMoviDetail(movieTop.id)" :title="movieTop.name_vn">
                                       <div class="item-link">
                                           <img :src="movieTop.image" class="lazy post-thumb" :alt="movieTop.name_vn" :title="movieTop.name_vn" />
                                           <span class="is_trailer">HD</span>
                                       </div>
                                       <p class="title">@{{ movieTop.name_vn }}</p>
                                   </a>
                                   <div class="viewsCount" style="color: #9d9d9d;">@{{ movieTop.view }} lượt xem</div>
                                   <div style="float: left;">
                                 <span class="user-rate-image post-large-rate stars-large-vang" style="display: block;/* width: 100%; */">
                                 <span style="width: 0%"></span>
                                 </span>
                                   </div>
                               </div>
                           </div>

                        </div>
                     </div>
                  </section>
                  <div class="clearfix"></div>
               </div>
            </aside>
         </div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
<script>
    var app = new Vue({
        el: '#load-movie-top-home',
        data: {
            movieTops: localStorage.getItem('moviesTop'),
            linkMovieDetail: "{{ route('movie-detail', ':id') }}"
        },
        computed: {
            movieTopConverts() {
                return JSON.parse(localStorage.getItem('moviesTop'))
            }
        },
        watch: {
            movieTops: {
                deep: true,
                handle(value) {
                    // console.log(value)
                }
            }
        },
        methods: {
            callMethod() {
                this.movieTops = localStorage.getItem('moviesTop')
            },
            rediarectMoviDetail(movieId) {
                this.linkMovieDetail = this.linkMovieDetail.replace(':id', movieId)
                return window.location.href = this.linkMovieDetail
            }
        }
    });
</script>
<script>
    $(document).ready(function () {
        getTopMovie()
    })
    $(document).on('click', '.ajax-tab', function () {
        var type = $(this).data("type");
        var movie_id = $(this).data('movie-id');
        var url = "{{ route('movie-search-by-dwm', ':key') }}"
        url = url.replace(':key', 'search_view=' + type)
        $.ajax({
            url: url,
            method: "GET",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                var movies = data.data.filter(function (item) {
                    return item.id !== movie_id
                })
                localStorage.setItem('moviesTop', JSON.stringify(movies))
                app.callMethod()
            }
        });
    });
    function getTopMovie() {
        var type = $(this).data("type");
        var movie_id = $(this).data('movie-id');
        var url = "{{ route('movie-search-by-dwm', ':key') }}"
        url = url.replace(':key', 'search_view=' + type)
        $.ajax({
            url: url,
            method: "GET",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                var movies = data.data.filter(function (item) {
                    return item.id !== movie_id
                })
                localStorage.setItem('moviesTop', JSON.stringify(movies))
                app.callMethod()
            }
        });
    }
</script>
@endsection
