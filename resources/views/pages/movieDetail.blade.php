@extends('layout')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="row container" id="wrapper">
        <div class="halim-panel-filter">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="yoast_breadcrumb hidden-xs"><span><span><a href="">{{ $movidetail->hastagMovie->name }}</a> » <span><a href="">{{ $movidetail->countryMovie->name }}</a> » <span class="breadcrumb_last" aria-current="page"> {{ $movidetail->name_vn }} </span></span></span></span></div>
                    </div>
                </div>
            </div>
            <div id="ajax-filter" class="panel-collapse collapse" aria-expanded="true" role="menu">
                <div class="ajax"></div>
            </div>
        </div>
        <main id="main-contents" class="col-xs-12 col-sm-12 col-md-8">
            <section id="content" class="test">
                <div class="clearfix wrap-content">
                    <div class="halim-movie-wrapper">
                        <div class="title-block">
                            <div id="bookmark-detail" class="bookmark-img-animation primary_ribbon" data-movie_bookmark="{{ $movidetail }}">
                                <div class="halim-pulse-ring"><i class="fas fa-bookmark" style="margin-left: 16px; margin-top: 14px; color: #ffcc00"></i></div>
                            </div>
                            <div class="title-wrapper" style="font-weight: bold;">
                                Bookmark
                            </div>
                        </div>
                        <div class="movie_info col-xs-12">
                            <div class="movie-poster col-md-3">
                                <img class="movie-thumb" src="{{ $movidetail->image }}" alt="GÓA PHỤ ĐEN">
                                <div class="bwa-content">
                                    <div class="loader"></div>
                                    <a href="{{route('movie-watch', $movidetail->id )}}" class="bwac-btn"><i class="fa fa-play"></i></a>
                                </div>
                            </div>
                            <div class="film-poster col-md-9">
                                <h1 class="movie-title title-1" style="display:block;line-height:35px;margin-bottom: -14px;color: #ffed4d;text-transform: uppercase;font-size: 18px;">{{ $movidetail->name_vn }}</h1>
                                <h2 class="movie-title title-2" style="font-size: 12px;">{{ $movidetail->name_en }}</h2>
                                <ul class="list-info-group">
                                    <li class="list-info-group-item"><span>Trạng Thái</span> : <span class="quality">HD</span><span class="episode">Vietsub</span></li>
                                    <li class="list-info-group-item"><span>Điểm IMDb</span> : <span class="imdb">7.2</span></li>
                                    <li class="list-info-group-item"><span>Thời lượng</span> : 133 Phút</li>
                                    <li class="list-info-group-item"><span>Thể loại</span> : <a href="" rel="category tag">{{ $movidetail->categoryMovie->name }}</a>
                                    </li>
                                    <li class="list-info-group-item"><span>Quốc gia</span> : <a href="" rel="tag">{{ $movidetail->countryMovie->name }}</a></li>
                                    <li class="list-info-group-item"><span>Đạo diễn</span> : <a class="director" rel="nofollow" href="https://phimhay.co/dao-dien/cate-shortland" title="Cate Shortland">Cate Shortland</a></li>
                                </ul>
                                <ul class="list-inline rating" title="Average Rating">
                                    @for($count=1; $count<=5; $count++)
                                        @php
                                            if($count<= $rating){
                                              $color = 'color:#ffcc00;'; //mau vang
                                            }
                                            else {
                                              $color = 'color:#ccc;'; //mau xam
                                            }
                                        @endphp
                                        <li title="star_rating"
                                            id="{{$movidetail->id}}-{{$count}}"
                                            data-index="{{$count}}"
                                            data-movie_id="{{$movidetail->id}}"
                                            data-rating="{{$rating}}"
                                            class="rating"
                                            style="cursor:pointer; {{$color}}
                                    font-size:30px;">&#9733;
                                        </li>

                                    @endfor

                                </ul>
                                <div class="movie-trailer hidden"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div id="halim_trailer"></div>
                    <div class="clearfix"></div>
                    <div class="section-bar clearfix">
                        <h2 class="section-title"><span style="color:#ffed4d">Nội dung phim</span></h2>
                    </div>
                    <div class="entry-content htmlwrap clearfix">
                        <div class="video-item halim-entry-box">
                            <article id="post-38424" class="item-content">
                                Phim <a href="{{route('movie-watch', $movidetail->id )}}">{{ $movidetail->name_vn }}</a> - 2021 - {{ $movidetail->countryMovie->name }}:
                                <p>{{ $movidetail->subscription }}</p>
                                <h5>Từ Khoá Tìm Kiếm:</h5>
                                <ul>
                                    <li>black widow vietsub</li>
                                </ul>
                            </article>
                        </div>
                    </div>
                </div>
            </section>
            <section class="related-movies">
                <div id="halim_related_movies-2xx" class="wrap-slider">
                    <div class="section-bar clearfix">
                        <h3 class="section-title"><span>CÓ THỂ BẠN MUỐN XEM</span></h3>
                    </div>
                    <div id="halim_related_movies-2" class="owl-carousel owl-theme related-film">
                        @foreach($movieByCategories as $movie)
                            <article class="thumb grid-item post-38498">
                                <div class="halim-item">
                                    <a class="halim-thumb" href="{{ route('movie-detail', $movie->id) }}" title="Đại Thánh Vô Song">
                                        <figure><img class="lazy img-responsive" src="{{ $movie->image }}"
                                                     alt="{{ $movie->name_vn }}" title="{{ $movie->name_vn }}"></figure>
                                        <span
                                            class="status">{{ $movie->quality == 1 ? 'HD' :( $movie->quality == 2 ? 720 : '480') }}</span><span
                                            class="episode"><i class="fa fa-play" aria-hidden="true"></i>Vietsub</span>
                                        <div class="icon_overlay"></div>
                                        <div class="halim-post-title-box">
                                            <div class="halim-post-title ">
                                                <p class="entry-title">{{ $movie->name_vn }}</p>
                                                <p class="original_title">{{ $movie->name_en }}</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </article>
                        @endforeach
                    </div>
                    <script>
                        jQuery(document).ready(function ($) {
                            var owl = $('#halim_related_movies-2');
                            owl.owlCarousel({
                                loop: true,
                                margin: 4,
                                autoplay: true,
                                autoplayTimeout: 4000,
                                autoplayHoverPause: true,
                                nav: true,
                                navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-angle-right"></i>'],
                                responsiveClass: true,
                                responsive: {0: {items: 2}, 480: {items: 3}, 600: {items: 4}, 1000: {items: 4}}
                            })
                        });
                    </script>
                </div>
            </section>
        </main>
        <aside id="sidebar" class="col-xs-12 col-sm-12 col-md-4"></aside>
    </div>

    <script type="text/javascript">

        function remove_background(movie_id) {
            for (var count = 1; count <= 5; count++) {
                $('#' + movie_id + '-' + count).css('color', '#ccc');
            }
        }

        //hover chuột đánh giá sao
        $(document).on('mouseenter', '.rating', function () {
            var index = $(this).data("index");
            var movie_id = $(this).data('movie_id');
            // alert(index);
            // alert(movie_id);
            remove_background(movie_id);
            for (var count = 1; count <= index; count++) {
                $('#' + movie_id + '-' + count).css('color', '#ffcc00');
            }
        });
        //nhả chuột ko đánh giá
        $(document).on('mouseleave', '.rating', function () {
            var index = $(this).data("index");
            var movie_id = $(this).data('movie_id');
            var rating = $(this).data("rating");
            remove_background(movie_id);
            //alert(rating);
            for (var count = 1; count <= rating; count++) {
                $('#' + movie_id + '-' + count).css('color', '#ffcc00');
            }
        });

        //click đánh giá sao
        $(document).on('click', '.rating', function () {
            var index = $(this).data("index");
            var movie_id = $(this).data('movie_id');
            $.ajax({
                url: "{{route('add-rating')}}",
                method: "POST",
                data: {index: index, movie_id: movie_id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    console.log(data)
                    if (data.data == 'done') {
                        alert("Bạn đã đánh giá " + index + " trên 5");
                        window.location.reload()
                    } else if (data == 'exist') {
                        alert("Bạn đã đánh giá phim này rồi,cảm ơn bạn nhé");
                    } else {
                        alert("Lỗi đánh giá");
                    }
                }
            });
        });

        $(document).on('click', '#bookmark-detail',  function () {
            let checkExits = true
            let bookmarkLists = []
            let itemId = $(this).data("movie_bookmark").id
            let bookmarks = JSON.parse(localStorage.getItem('bookmark-list'));
            if (bookmarks) {
                bookmarkLists = bookmarks
                bookmarks.map(function (item) {
                    if (item.id === itemId) {
                        checkExits = false
                        alert('Ban da them video nay vao tu phim truoc do')
                    }
                }, itemId)
            }
            if (checkExits) {
                bookmarkLists.push($(this).data("movie_bookmark"))
                localStorage.setItem('bookmark-list', JSON.stringify(bookmarkLists))
                alert('Them video vao tu phim thanh cong')
                window.location.reload()
            }
        })
    </script>
@endsection
