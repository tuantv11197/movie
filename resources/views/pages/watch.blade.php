@extends('layout')
@section('content')
    <div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
<div class="row container" id="wrapper">
         <div class="halim-panel-filter">
            <div class="panel-heading">
               <div class="row">
                  <div class="col-xs-6">
                  <div class="yoast_breadcrumb hidden-xs"><span><span><a href="">{{ $movidetail->hastagMovie->name }}</a> » <span><a href="">{{ $movidetail->countryMovie->name }}</a> » <span class="breadcrumb_last" aria-current="page"> {{ $movidetail->name_vn }}</span></span></span></span></div>
                  </div>
               </div>
            </div>
            <div id="ajax-filter" class="panel-collapse collapse" aria-expanded="true" role="menu">
               <div class="ajax"></div>
            </div>
         </div>
         <main id="main-contents" class="col-xs-12 col-sm-12 col-md-8">
            <section id="content" class="test">
               <div class="clearfix wrap-content">

                  <!-- <iframe width="100%" height="500" src="https://www.youtube.com/embed/r958O404e4U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                  <iframe src="{{ $movidetail->episodes[0]->link_movie }}" width="100%" height="500" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                  <div class="button-watch">
                     <ul class="halim-social-plugin col-xs-4 hidden-xs">
                        <li class="fb-like" data-href="" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></li>
                     </ul>
{{--                     <ul class="col-xs-12 col-md-8">--}}
{{--                        <div id="autonext" class="btn-cs autonext">--}}
{{--                           <i class="icon-autonext-sm"></i>--}}
{{--                           <span><i class="hl-next"></i> Autonext: <span id="autonext-status">On</span></span>--}}
{{--                        </div>--}}
{{--                        <div id="explayer" class="hidden-xs"><i class="hl-resize-full"></i>--}}
{{--                           Expand--}}
{{--                        </div>--}}
{{--                        <div id="toggle-light"><i class="hl-adjust"></i>--}}
{{--                           Light Off--}}
{{--                        </div>--}}
{{--                        <div id="report" class="halim-switch"><i class="hl-attention"></i> Report</div>--}}
{{--                        <div class="luotxem"><i class="hl-eye"></i>--}}
{{--                           <span>1K</span> lượt xem--}}
{{--                        </div>--}}
{{--                        <div class="luotxem">--}}
{{--                           <a class="visible-xs-inline" data-toggle="collapse" href="#moretool" aria-expanded="false" aria-controls="moretool"><i class="hl-forward"></i> Share</a>--}}
{{--                        </div>--}}
{{--                     </ul>--}}
                  </div>
                  <div class="collapse" id="moretool">
                     <ul class="nav nav-pills x-nav-justified">
                        <li class="fb-like" data-href="" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></li>
                        <div class="fb-save" data-uri="" data-size="small"></div>
                     </ul>
                  </div>
                  <div class="d-flex">
                    <div id="bookmark-watch" style="top: -8px" class="bookmark-img-animation primary_ribbon" data-movie_bookmark="{{ $movidetail }}">
                       <div class="halim-pulse-ring"><i class="far fa-bookmark" style="margin-left: 16px; margin-top: 14px; color: #ffcc00"></i></div>
                    </div>
                     <div class="">
                        <h2 class="entry-title"><a href="" title="{{ $movidetail->name_vn }}" class="tl">{{ $movidetail->name_vn }}</a></h2>
                     </div>
                  </div>
                  <div class="entry-content htmlwrap clearfix collapse" id="expand-post-content">
                     <article id="post-37976" class="item-content post-37976"></article>
                  </div>
                  <div class="clearfix"></div>
                  <div class="text-center">
                     <div id="halim-ajax-list-server"></div>
                  </div>
                  <div id="halim-list-server">
                     <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active server-1"><a href="#server-0" aria-controls="server-0" role="tab" data-toggle="tab"><i class="hl-server"></i> Vietsub</a></li>
                     </ul>
                      @if($movidetail->hastag_movie_id == 2)
                          <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active server-1" id="server-0">
                                  <div class="halim-server">
                                      <ul class="halim-list-eps">
                                          @foreach($episodes as $episode)
                                              <li class="halim-episode"><span class="halim-btn halim-btn-2 halim-info-1-1 box-shadow {{ $episode == $episodeId[0] ? 'active' : '' }}" data-post-id="37976" data-server="1" data-episode="{{$episode}}" data-position="first" data-embed="0" data-title="" data-h1=""><a href="{{route('movie-watch', $movidetail->id ) . '/' . $episode}}">{{ $episode }}</a></span></li>
                                          @endforeach
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      @endif
                  </div>
                   <br>
                   <div id="comment-layout">
                       <section>
                           <div class="container-comment">
                               <div class="row layout-main">
                                   <div class="col-lg-4 col-md-5  col-sm-4 offset-md-1 offset-sm-1 col-12 mt-4 form-cm" :class="comments.length === 0 ? 'custom-width' : ''">
                                       <form id="algin-form">
                                           <div class="form-group">
                                               <h4>Để lại bình luận</h4>
                                               <label for="message">Bình luận</label>
                                               <label style="color: red">@{{ messageValidate }}</label>
                                               <textarea v-model="message" name="msg" id=""msg cols="30" rows="5" class="form-control" style="background-color: black;"></textarea>
                                           </div>
                                           <div class="form-group">
                                               <button type="button" id="post" @click="postComment()" class="btn">Bình Luận</button>
                                           </div>
                                       </form>
                                   </div>
                                   <div id="box-comment-content" class="box-comment-content col-sm-5 col-md-6 col-12 pb-4" :class="comments.length === 0 ? 'hide-box-cm' : ''">
                                       <div v-for="comment in comments" class="comment mt-4 text-justify float-left mb-2">
                                           <div>
                                               <img :src="comment.user.image_url" alt="" class="rounded-circle" width="40" height="40">
                                               <h4>@{{ comment.user.name }}</h4>
                                           </div>
                                           <div class="content-chill">
                                               <p>@{{ comment.content }}</p>
                                               <span>@{{ comment.time }}</span>
                                           </div>
                                       </div>
{{--                                       <div class="text-justify darker mt-4 float-right">--}}
{{--                                           <img src="https://i.imgur.com/CFpa3nK.jpg" alt="" class="rounded-circle" width="40" height="40">--}}
{{--                                           <h4>Rob Simpson</h4>--}}
{{--                                           <span>- 20 October, 2018</span>--}}
{{--                                           <br>--}}
{{--                                           <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusamus numquam assumenda hic aliquam vero sequi velit molestias doloremque molestiae dicta?</p>--}}
{{--                                       </div>--}}
                                   </div>
                               </div>
                           </div>
                       </section>
                   </div>
                  <div class="htmlwrap clearfix">
                     <div id="lightout"></div>
                  </div>
            </section>
            <section class="related-movies">
            <div id="halim_related_movies-2xx" class="wrap-slider">
            <div class="section-bar clearfix">
            <h3 class="section-title"><span>CÓ THỂ BẠN MUỐN XEM</span></h3>
            </div>
            <div id="halim_related_movies-2" class="owl-carousel owl-theme related-film">
                @foreach($movieByCategories as $movie)
                    <article class="thumb grid-item post-38494">
                        <div class="halim-item">
                            <a class="halim-thumb" href="{{ route('movie-detail', $movie->id) }}" title="{{ $movie->name_vn }}">
                                <figure><img class="lazy img-responsive" src="{{ $movie->image }}" alt="{{ $movie->name_vn }}" title="{{ $movie->name_vn }}"></figure>
                                <span class="status">{{ $movie->quality == 1 ? 'HD' :( $movie->quality == 2 ? 720 : '480') }}</span><span class="episode"><i class="fa fa-play" aria-hidden="true"></i>Vietsub</span> <div class="icon_overlay"></div>
                                <div class="halim-post-title-box">
                                    <div class="halim-post-title ">
                                        <p class="entry-title">{{ $movie->name_vn }}</p><p class="original_title">{{ $movie->name_en }}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </article>
                @endforeach
            </div>
            <script>
               jQuery(document).ready(function($) {
               var owl = $('#halim_related_movies-2');
               owl.owlCarousel({loop: true,margin: 4,autoplay: true,autoplayTimeout: 4000,autoplayHoverPause: true,nav: true,navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-angle-right"></i>'],responsiveClass: true,responsive: {0: {items:2},480: {items:3}, 600: {items:4},1000: {items: 4}}})});
            </script>
            </div>
            </section>
         </main>
        <aside id="sidebar" class="col-xs-12 col-sm-12 col-md-4">
               <div id="halim_tab_popular_videos-widget-7" class="widget halim_tab_popular_videos-widget">
                  <div class="section-bar clearfix">
                     <div class="section-title">
                        <span>Top Views</span>
                         <div id="test-vue">
                             <ul class="halim-popular-tab" role="tablist">
                                 <li role="presentation" class="active">
                                     <a class="ajax-tab" role="tab" data-toggle="tab" data-showpost="10" data-movie-id="{{ $movidetail->id }}" data-type="day">Day</a>
                                 </li>
                                 <li role="presentation">
                                     <a class="ajax-tab" role="tab" data-toggle="tab" data-showpost="10" data-movie-id="{{ $movidetail->id }}" data-type="week">Week</a>
                                 </li>
                                 <li role="presentation">
                                     <a class="ajax-tab" role="tab" data-toggle="tab" data-showpost="10" data-movie-id="{{ $movidetail->id }}" data-type="month">Month</a>
                                 </li>
                                 <li role="presentation">
                                     <a class="ajax-tab" role="tab" data-toggle="tab" data-showpost="10" data-movie-id="{{ $movidetail->id }}" data-type="all">All</a>
                                 </li>
                             </ul>
                         </div>
                     </div>
                  </div>
                  <section class="tab-content">
                     <div role="tabpanel" class="tab-pane active halim-ajax-popular-post">
                        <div id="load-movie-top" class="popular-post">
                                <div v-for="movieTop in JSON.parse(movieTops)" class="item post-37176">
                                    <a @click="rediarectMoviDetail(movieTop.id)" :title="movieTop.name_vn">
                                        <div class="item-link">
                                            <img :src="movieTop.image" class="lazy post-thumb" :alt="movieTop.name_vn" :title="movieTop.name_vn" />
                                            <span class="is_trailer">HD</span>
                                        </div>
                                        <p class="title">@{{ movieTop.name_vn }}</p>
                                    </a>
                                    <div class="viewsCount" style="color: #9d9d9d;">@{{ movieTop.view }} lượt xem</div>
                                    <div style="float: left;">
                                 <span class="user-rate-image post-large-rate stars-large-vang" style="display: block;/* width: 100%; */">
                                 <span style="width: 0%"></span>
                                 </span>
                                    </div>
                                </div>
                        </div>
                     </div>
                  </section>
                  <div class="clearfix"></div>
               </div>
            </aside>
      </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        var app = new Vue({
            el: '#load-movie-top',
            data: {
                movieTops: localStorage.getItem('moviesTop'),
                linkMovieDetail: "{{ route('movie-detail', ':id') }}"
            },
            computed: {
                movieTopConverts() {
                    return JSON.parse(localStorage.getItem('moviesTop'))
                }
            },
            watch: {
                movieTops: {
                    deep: true,
                    handle(value) {
                        // console.log(value)
                    }
                }
            },
            methods: {
                callMethod() {
                    this.movieTops = localStorage.getItem('moviesTop')
                },
                rediarectMoviDetail(movieId) {
                    this.linkMovieDetail = this.linkMovieDetail.replace(':id', movieId)
                    return window.location.href = this.linkMovieDetail
                }
            }
        });
        var comment = new Vue({
            el: '#comment-layout',
            data: {
                comments: [],
                message: '',
                messageValidate: ''
            },
            created() {
                window.Echo.channel('chat_' + window.location.href.split('/')[4]).listen('CommentEvent', (data) => {
                    this.comments.push(data)
                    this.message = ''
                })
                this.getComment()
            },
            mounted() {
                // document.addEventListener("DOMContentLoaded", function() {
                //     var scrollableDiv = document.getElementById("box-comment-content");
                //     if (scrollableDiv) {
                //         scrollableDiv.scrollTop = scrollableDiv.scrollHeight;
                //     }
                // });
                // document.getElementById('box-comment-content').scrollTop = document.getElementById('box-comment-content').scrollHeight;
            },
            methods: {
                async getComment() {
                    console.log(window.location.href.split('/').pop())
                    try {
                        let movieId = window.location.href.split('/')[4]
                        const response = await axios.get('/get-comment', {
                            params: { movie_id: movieId }
                        })
                        this.comments = response.data.data
                        var element = document.querySelector('#box-comment-content');
                        element.scrollTop = element.scrollHeight;
                    } catch (error) {
                        console.log(error.response)
                    }
                },
                async postComment() {
                    this.messageValidate = ''
                    let movieId = window.location.href.split('/')[4]
                    try {
                        const response = await axios.post('/comment', { content: this.message, movie_id: movieId })
                    } catch (error) {
                        console.log(error.response)
                        if (error.response.data.code === 403) {
                            alert(error.response.data.messages)
                            window.location.href = "{{ route('login.view') }}"
                        }
                        if(error.response.status === 422) {
                            this.messageValidate = error.response.data.message
                        }
                    }
                }
            }
        })
    </script>
    <script>
        $(document).ready(function () {
            getTopMovie()
        })
        $(document).on('click', '.ajax-tab', function () {
            var type = $(this).data("type");
            var movie_id = $(this).data('movie-id');
            var url = "{{ route('movie-search-by-dwm', ':key') }}"
            url = url.replace(':key', 'search_view=' + type)
            $.ajax({
                url: url,
                method: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    var movies = data.data.filter(function (item) {
                        return item.id !== movie_id
                    })
                    localStorage.setItem('moviesTop', JSON.stringify(movies))
                    app.callMethod()
                }
            });
        });
        function getTopMovie() {
            var type = $(this).data("type");
            var movie_id = $(this).data('movie-id');
            var url = "{{ route('movie-search-by-dwm', ':key') }}"
            url = url.replace(':key', 'search_view=' + type)
            $.ajax({
                url: url,
                method: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    var movies = data.data.filter(function (item) {
                        return item.id !== movie_id
                    })
                    localStorage.setItem('moviesTop', JSON.stringify(movies))
                    // app.callMethod()
                }
            });
        }

        $(document).on('click', '#bookmark-watch',  function () {
            let checkExits = true
            let bookmarkLists = []
            let itemId = $(this).data("movie_bookmark").id
            let bookmarks = JSON.parse(localStorage.getItem('bookmark-list'));
            if (bookmarks) {
                bookmarkLists = bookmarks
                bookmarks.map(function (item) {
                    if (item.id === itemId) {
                        checkExits = false
                        alert('Ban da them video nay vao tu phim truoc do')
                    }
                }, itemId)
            }
            if (checkExits) {
                bookmarkLists.push($(this).data("movie_bookmark"))
                localStorage.setItem('bookmark-list', JSON.stringify(bookmarkLists))
                alert('Them video vao tu phim thanh cong')
                window.location.reload()
            }
        })
    </script>
    <style>
        .hide-box-cm {
            display: none;
        }
        .form-cm {
            width: 38%;
        }
        .custom-width {
            margin-top: 40px;
            width: 100% !important;
        }
        .content-chill {
            padding: 0 45px;
        }
        .box-comment-content {
            width: 60%;
            height: 299px;
            overflow: auto;
        }
        .container-comment {
            padding-left: 0;
            padding-right: 0;
        }
        .container-comment h1 {
            margin-top: -11px;
        }
        .navbar-nav{
            width: 100%;
        }
        @media(max-width:414px){
            .form-cm {
                width: 100%;
            }
            #box-comment-content {
                margin-top: 15px;
                width: 100%;
            }
        }

        @media(min-width:568px){
            .end{
                margin-left: auto;
            }
        }

        @media(max-width:768px){
            #post{
                width: 100%;
            }
        }
        #clicked{
            padding-top: 1px;
            padding-bottom: 1px;
            text-align: center;
            width: 100%;
            background-color: #ecb21f;
            border-color: #a88734 #9c7e31 #846a29;
            color: black;
            border-width: 1px;
            border-style: solid;
            border-radius: 13px;
        }

        #profile{
            background-color: unset;

        }

        #post{
            margin: 10px;
            padding: 6px;
            padding-top: 2px;
            padding-bottom: 2px;
            text-align: center;
            background-color: #ecb21f;
            border-color: #a88734 #9c7e31 #846a29;
            color: black;
            border-width: 1px;
            border-style: solid;
            border-radius: 13px;
            width: 50%;
        }

        body{
            background-color: black;
        }

        #nav-items li a,#profile{
            text-decoration: none;
            color: rgb(224, 219, 219);
            background-color: black;
        }


        .comments{
            margin-top: 5%;
            margin-left: 20px;
        }

        .darker{
            border: 1px solid #ecb21f;
            background-color: black;
            float: right;
            border-radius: 5px;
            padding-left: 40px;
            padding-right: 30px;
            padding-top: 10px;
        }

        .comment{
            border: 1px solid rgba(16, 46, 46, 1);
            background-color: rgba(16, 46, 46, 0.973);
            float: left;
            border-radius: 5px;
            display: block;
            width: 100%;
            margin-bottom: 10px;

        }
        .comment h4,.comment span,.darker h4,.darker span{
            display: inline;
        }

        .comment p,.comment span,.darker p,.darker span{
            color: rgb(184, 183, 183);
        }

        h1,h4{
            color: white;
            font-weight: bold;
        }
        label{
            color: rgb(212, 208, 208);
        }

        #align-form{
            margin-top: 20px;
        }
        .form-group p a{
            color: white;
        }

        #checkbx{
            background-color: black;
        }

        #darker img{
            margin-right: 15px;
            position: static;
        }

        .form-group input,.form-group textarea{
            background-color: black;
            border: 1px solid rgba(16, 46, 46, 1);
            border-radius: 12px;
        }

        form{
            border: 1px solid rgba(16, 46, 46, 1);
            background-color: rgba(16, 46, 46, 0.973);
            border-radius: 5px;
            padding: 20px;
        }
    </style>
@endsection
