<?php

namespace Database\Seeders;

use App\Models\CategoryMovie;
use App\Models\CountryMovie;
use App\Models\Episode;
use App\Models\HastagMoview;
use App\Models\Movie;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
         \App\Models\User::factory(10)->create();
    }
}
