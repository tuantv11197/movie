<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();
            $table->integer('hastag_movie_id')->comment('loai phim la phim bo hay phim chieu rap hay phim le');
            $table->integer('category_id');
            $table->string('name_vn');
            $table->string('name_en');
            $table->enum('quality', [1, 2, 3])->comment('1: HD, 2: 720, 3: 480');
            $table->tinyInteger('is_hot')->default(1)->comment('1: hot');
            $table->integer('episode_id')->comment('tap phim');
            $table->longText('subscription');
            $table->text('slug');
            $table->integer('country_id');
            $table->string('image');
            $table->tinyInteger('is_new');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
